package com.alltrell.readsgood.readsgood.service;

import com.alltrell.readsgood.readsgood.exception.ItemNotFoundException;
import com.alltrell.readsgood.readsgood.model.Person;
import com.alltrell.readsgood.readsgood.repo.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    private final PersonRepo personRepo;

    @Autowired
    public PersonService(PersonRepo personRepo) { this.personRepo = personRepo; }

    public Person addPerson(Person person) { return personRepo.save(person); }

    public List<Person> findAllPersons() { return personRepo.findAll(); }

    public Person updatePerson(Person person) { return personRepo.save(person); }

    public Person findPersonById(Long id) {
        return personRepo.findPersonById(id)
                .orElseThrow(() -> new ItemNotFoundException(String.format("Person by id %s was not found", id)));
    }

    public Person findPersonByEmail(String email) {
        return personRepo.findPersonByEmail(email).orElse(null);
    }

    public void deletePerson(Long id) { personRepo.deletePersonById(id); }
}
