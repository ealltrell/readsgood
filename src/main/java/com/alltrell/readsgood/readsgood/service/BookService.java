package com.alltrell.readsgood.readsgood.service;

import com.alltrell.readsgood.readsgood.exception.ItemNotFoundException;
import com.alltrell.readsgood.readsgood.model.Book;
import com.alltrell.readsgood.readsgood.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private final BookRepo bookRepo;

    @Autowired
    public BookService(BookRepo bookRepo) { this.bookRepo = bookRepo; }

    public Book addBook(Book book) { return bookRepo.save(book); }

    public List<Book> findAllBooks() { return bookRepo.findAll(); }

    public Book updateBook(Book book) { return bookRepo.save(book); }

    public Book findBookById(Long id) {
        return bookRepo.findBookById(id).orElseThrow(() -> new ItemNotFoundException(String.format("Book by id %s was not found", id)));
    }

//    public Book findBookByAuthorId(Long id) {
//
//        return bookRepo.findBookByAuthorId(id).orElse(null);
//    }

    public void deleteBook(Long id) { bookRepo.deleteBookById(id); }
}
