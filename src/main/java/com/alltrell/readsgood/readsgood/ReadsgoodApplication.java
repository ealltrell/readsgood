package com.alltrell.readsgood.readsgood;

import com.alltrell.readsgood.readsgood.model.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@SpringBootApplication
public class ReadsgoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadsgoodApplication.class, args);
	}
}
