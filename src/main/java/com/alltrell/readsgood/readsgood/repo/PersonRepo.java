package com.alltrell.readsgood.readsgood.repo;

import com.alltrell.readsgood.readsgood.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonRepo extends JpaRepository<Person, Long> {

    void deletePersonById(Long id);

    Optional<Person> findPersonById(Long id);

    Optional<Person> findPersonByEmail(String email);
}
