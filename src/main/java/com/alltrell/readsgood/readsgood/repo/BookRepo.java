package com.alltrell.readsgood.readsgood.repo;

import com.alltrell.readsgood.readsgood.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookRepo extends JpaRepository<Book, Long> {

    void deleteBookById(Long id);

    Optional<Book> findBookById(Long id);

//    Optional<Book> findBookByAuthorId(Long id);
}
