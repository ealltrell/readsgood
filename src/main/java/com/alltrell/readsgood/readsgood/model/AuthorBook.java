package com.alltrell.readsgood.readsgood.model;

import javax.persistence.*;

@Entity
public class AuthorBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    @OneToOne
    @JoinColumn(name = "book_id")
    private Book bookId;
    @OneToOne
    @JoinColumn(name = "author_id")
    private Person authorId;

    public AuthorBook() {
    }

    public AuthorBook(Book bookId, Person authorId) {
        this.bookId = bookId;
        this.authorId = authorId;
    }

    public Long getId() {
        return id;
    }
}
