package com.alltrell.readsgood.readsgood.model;

import javax.persistence.*;

@Entity
public class ShelfBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    @OneToOne
    @JoinColumn(name = "book_id")
    private Book bookId;
    @OneToOne
    @JoinColumn(name = "shelf_id")
    private Shelf shelfId;

    public ShelfBook() {
    }

    public ShelfBook(Book bookId, Shelf shelfId) {
        this.bookId = bookId;
        this.shelfId = shelfId;
    }

    public Long getId() {
        return id;
    }
}
